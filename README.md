# Linkcheck-Docker

Docker image for [LinkCheck](https://github.com/filiph/linkcheck/) based on Ubuntu (18.04).

## Rationale

The suggested [Dockerfile by Filiph](https://github.com/filiph/linkcheck/blob/master/Dockerfile) crashes when used
in Gitlab-CI (on gitlab.com), see
[Docker image in Gitlab-CI crashes with error: FormatException: Could not find an option or flag "-c"](https://github.com/filiph/linkcheck/issues/85)

That's why in the meantime I created this image based on Ubuntu (not on the whole Dart dev stack) :)

## How to build

```shell script
docker build -t linkcheck . 
```

## Usage

```shell script
docker run linkcheck-ubuntu <url>
```

### Build and upload docker image to gitlab.com

From Gitlab documentation:

> If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username
> and password. If you have [Two-Factor Authentication](https://gitlab.com/help/user/profile/account/two_factor_authentication)
> enabled, use a [Personal Access Token](https://gitlab.com/help/user/profile/personal_access_tokens) instead of a
> password.

```shell script
# token is the Personal Access Token from Gitlab.com
echo <token> | docker login registry.gitlab.com -u <username> --password-stdin
docker build -t registry.gitlab.com/asqatasun/linkcheck-docker/linkcheck:2.0.19 .
docker push registry.gitlab.com/asqatasun/linkcheck-docker/linkcheck:2.0.19
```

## Usage in Gitlab-CI

In your `.gitlab-ci.yml` file add:

```yaml
linkcheck:
  variables:
    URL: "https://doc.asqatasun.org/"
  image:
    name: registry.gitlab.com/asqatasun/linkcheck-docker/linkcheck:2.0.19
  script:
    - linkcheck "${URL}"
```