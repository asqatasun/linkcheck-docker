FROM ubuntu:18.04

ENV LINKCHECK_VERSION="2.0.19"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y wget && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    wget -q https://github.com/filiph/linkcheck/releases/download/${LINKCHECK_VERSION}/linkcheck-${LINKCHECK_VERSION}-linux-x64.tar.gz && \
    tar xvfz linkcheck-${LINKCHECK_VERSION}-linux-x64.tar.gz

ENTRYPOINT [ "linkcheck/linkcheck" ]